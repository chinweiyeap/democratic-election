package democratic;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ElectorRMI extends Remote {

    void handleMessage(DemElector.MessageType type, int round, int id, String vote) throws RemoteException;
}
