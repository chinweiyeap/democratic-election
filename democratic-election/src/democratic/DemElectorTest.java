package democratic;

import org.junit.Test;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;


public class DemElectorTest {

    public static final int RMI_PORT = 1100;

    private int countFinished(DemElector[] electors) {
        int counter = 0;
        for (int i = 0; i < electors.length; i++) {
            if (electors[i] != null) {
                if (electors[i].done) {
                    counter++;
                }
            }
        }

        return counter;
    }

    private void wait(DemElector[] electors, int wanted) {
        int to = 10;
        for (int i = 0; i < 30000; i++) {
            if (countFinished(electors) >= wanted) {
                break;
            }
            try {
                Thread.sleep(to);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (to < 1000) {
                to = to * 2;
            }
        }
    }

    private void cleanup(DemElector[] electors) {
        for (DemElector e : electors) {
            if (e != null) {
                e.kill();
            }
        }
    }

    private DemElector[] initElectors(int peers, int failures, int candidates) throws RemoteException, AlreadyBoundException {
        DemElector[] electors = new DemElector[peers];

        for (int i = 0; i < peers; i++) {
            DemElector.FailureType type = i < failures ? DemElector.FailureType.RANDOM_SIMPLE : DemElector.FailureType.NOFAILURE;
            electors[i] = new DemElector(i, peers, failures, candidates, type, RMI_PORT);
        }

        return electors;
    }

    private static void startByzantine(DemElector elector, boolean useRing) {
        try {
            if (useRing) {
                elector.startElectionWithLeader(0);
            } else {
                elector.startElection(0);
            }
        } catch (Exception e) {
            System.out.println("Failure!");
            e.printStackTrace();
        }
    }

    @Test
    public void basicTest() {
        final int peers = 75;
        final int failures = 0;
        final int candidates = 4;

        System.out.printf("Begin basic test with %d peers and %d candidates\n", peers, candidates);
        DemElector[] electors = new DemElector[peers];

        try {
            electors = initElectors(peers, failures, candidates);

            Thread threads[] = new Thread[peers];
            for (int i = 0; i < peers; i++) {
                final DemElector e = electors[i];
                Runnable task = () -> startByzantine(e, false);
                threads[i] = new Thread(task);
                threads[i].start();
            }

            wait(electors, electors.length);
        } catch (Exception e) {
            System.out.println("Failure!");
            e.printStackTrace();
        } finally {
            cleanup(electors);
        }
    }

    @Test
    public void ringTest() {
        final int peers = 75;
        final int failures = 0;
        final int candidates = 4;
        DemElector[] electors = new DemElector[peers];

        try {
            electors = initElectors(peers, failures, candidates);

            Thread threads[] = new Thread[peers];
            for (int i = 0; i < peers; i++) {
                final DemElector e = electors[i];
                Runnable task = () -> startByzantine(e, true);
                threads[i] = new Thread(task);
                threads[i].start();
            }

            wait(electors, electors.length);
        } catch (Exception e) {
            System.out.println("Failure!");
            e.printStackTrace();
        } finally {
            cleanup(electors);
        }
    }

}
