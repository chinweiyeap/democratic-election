package democratic;

import com.sun.tools.corba.se.idl.constExpr.Not;
import javafx.util.Pair;
import org.jasypt.util.text.BasicTextEncryptor;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import static democratic.DemElector.MessageType.*;

public class DemElector implements ElectorRMI {

    public boolean done = false;
    private int peers; // hostname
    private int me; // index into peers[]
    private int round;
    private int candidates;
    private int failures;
    private int basePort;
    private int leader = -1;
    private FailureType type;
    private Map<Integer, String> encBallot = new HashMap<>();
    private Map<Integer, List<Integer>> ballot = new HashMap<>();
    private List<Message> messages;
    private List<Integer> result;
    private String key;

    private Registry registry;
    private ElectorRMI stub;

    public enum MessageType {
        EXCHANGE,
        NOTIFICATION,
        PROPOSAL,
        KEY,
        ELECTION,
        ELECTED,
        VOTE,
        RESULT
    }

    public enum FailureType {
        NOFAILURE,
        OMISSION,
        RANDOM_SIMPLE, // broadcast the same random value to all nodes
        RANDOM_COMPLEX, // different random values are sent to individual nodes
        BOTH // omit message half of the time and use RANDOM_COMPLEX when sending messages
    }

    static final class Message {
        public final MessageType type;
        public final int round;
        public final int peer;
        public final String vote;
        public List<Integer> ballot;
        private Message(MessageType type, int round, int peer, String vote) {
            this.type = type;
            this.round = round;
            this.peer = peer;
            this.vote = vote;
        }
    }

    static final class MajorityTally {
        public final String majority;
        public final long tally;
        private MajorityTally(String majority, long tally) {
            this.majority = majority;
            this.tally = tally;
        }
    }

    public DemElector(int me, int peers, int failures, int candidates, FailureType type, int portNumber) throws RemoteException, AlreadyBoundException {
        this.peers = peers;
        this.me = me;
        this.round = 0;
        this.failures = failures;
        this.candidates = candidates;
        this.type = type;
        this.messages = new ArrayList<>();
        this.basePort = portNumber;
//        registry = LocateRegistry.getRegistry(portNumber);
//        registry.bind(Integer.toString(me), this);

        // register peers, do not modify this part
        try {
            System.setProperty("java.rmi.server.hostname", "127.0.0.1");
            registry = LocateRegistry.createRegistry(basePort + me);
            stub = (ElectorRMI) UnicastRemoteObject.exportObject(this, basePort + me);
            registry.rebind("DemElector", stub);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        System.out.printf("[%d] initiated with type = %s\n", me, type.toString());
    }

    public void startElection(int round) throws RemoteException, InterruptedException, MalformedURLException {

        Random rn = new Random();
        List<Integer> currentVotes = createRandomBallot(rn);

        key = createEncryptionKey();

        String myVote = encrypt(currentVotes, key);
//        Thread.sleep(500);

//        List<String> voteStrings = currentVotes.stream()
//                .map(String::valueOf)
//                .collect(Collectors.toList());
//        String toEncrypt = String.join(",", voteStrings);

//        System.out.printf("[%d] Encrypt - vote=%s, result=%s, key=%s, \n", me, toEncrypt, myVote, key);
        broadcast(EXCHANGE, me, myVote);

        await(EXCHANGE, -1);

        /* BYZANTINE AGREEMENT ON BALLOT */
        for (int peer = 0; peer < candidates; peer++) {

            String currentVote = encBallot.get(peer);
            // Add random delay
            Thread.sleep(rn.nextInt(500));
            prepareNewRound();

            /* NOTIFICATION PHASE */
            if (type == FailureType.OMISSION) {
                // Do nothing
            } else if (type == FailureType.RANDOM_SIMPLE) {
                List<Integer> ballot = createRandomBallot(rn);
                String incorrectVote = encrypt(ballot, key);
                broadcast(NOTIFICATION, peer, incorrectVote);
            } else if (type == FailureType.RANDOM_COMPLEX) {
                randomBroadcast(NOTIFICATION, peer, rn);
            } else if (type == FailureType.BOTH) {
                if (rn.nextInt(2) == 0) {
                    randomBroadcast(NOTIFICATION, peer, rn);
                }
            } else {
                broadcast(NOTIFICATION, peer, currentVote);
            }

            await(NOTIFICATION, peer);

            /* PROPOSAL PHASE */
            MajorityTally proposalResult;
            synchronized (messages) {
                proposalResult = getMajorityTally(messages, NOTIFICATION, peer, round);
            }

//                cleanupMessages(NOTIFICATION);

            if (type == FailureType.OMISSION) {
                // Do nothing
            } else if (type == FailureType.RANDOM_SIMPLE) {
                List<Integer> ballot = createRandomBallot(rn);
                String incorrectVote = encrypt(ballot, key);
                broadcast(PROPOSAL, peer, incorrectVote);
            } else if (type == FailureType.RANDOM_COMPLEX) {
                randomBroadcast(PROPOSAL, peer, rn);
            } else if (type == FailureType.BOTH) {
                if (rn.nextInt(2) == 0) {
                    randomBroadcast(PROPOSAL, peer, rn);
                }
            } else {
                if (proposalResult.tally > (peers + failures) / 2) {
                    broadcast(PROPOSAL, peer, proposalResult.majority);
                } else {
                    broadcast(PROPOSAL, peer, "");
                }
            }

            await(PROPOSAL, peer);

            /* DECISION PHASE */
            MajorityTally decision;
            synchronized (messages) {
                decision = getMajorityTally(messages, PROPOSAL, peer, round);
            }

            if (decision.tally > 3 * failures) {
                synchronized (encBallot) {
                    encBallot.put(peer, decision.majority);
                    break;
                }
            } else {
                System.out.printf("[%d] Unable to decide for %d - tally=%d\n", me, peer, decision.tally);
            }
        }

        broadcast(KEY, me, key);
        await(KEY, -1);

        List<List<Integer>> values = new ArrayList<>(ballot.values());

        List<Integer> result = prunedKY(values);
        System.out.printf("[%d] Result: %s\n", me, result.toString());

        done = true;
    }

    public void startElectionWithLeader(int round) throws RemoteException, InterruptedException, MalformedURLException, NotBoundException {
        // Steps:
        // 1) Create messages
        // 2) Begin ring leader election
        // 3) Upon receiving leader decision, send vote to leader
        // 4) Leader counts votes, sends results to everyone

        send(ELECTION, nextPeer(), round, me, String.valueOf(me));

        await(ELECTED, me);


        Random rn = new Random();
        List<Integer> currentVotes = createRandomBallot(rn);

        List<String> voteStrings = currentVotes.stream()
                .map(String::valueOf)
                .collect(Collectors.toList());
        String myVote = String.join(",", voteStrings);
        singleMessage(VOTE, leader, round, me, myVote);
    }

    public void kill() {
        if (this.registry != null) {
            try {
                UnicastRemoteObject.unexportObject(this.registry, true);
            } catch (Exception e) {
                System.out.println("None reference");
            }
        }
    }

    public void handleMessage(MessageType type, int round, int id, String value) throws RemoteException {
        try {
            if (type == KEY) {
                synchronized (ballot) {
                    String vote = encBallot.get(id);
//                System.out.printf("[%d] Decrypt - peer=%d, vote=%s, key=%s\n", me, id, vote, value);
                    List<Integer> votes = decrypt(vote, value);
                    ballot.put(id, votes);
                }

            } else if (type == RESULT) {
                List<String> voteStrings = Arrays.asList(value.split(","));
                result = voteStrings.stream()
                        .map(Integer::valueOf)
                        .collect(Collectors.toList());


                System.out.printf("[%d] Result: %s\n", me, result.toString());

                done = true;
            } else if (type == VOTE) {
                synchronized (ballot) {
                    List<String> voteStrings = Arrays.asList(value.split(","));
                    List<Integer> vote = voteStrings.stream()
                            .map(Integer::valueOf)
                            .collect(Collectors.toList());

                    ballot.put(id, vote);
                    if (ballot.size() >= peers) {
                        countVotes();
                    }
                }
            } else if (type == ELECTION) {
                int leader = Integer.valueOf(value);
                if (leader > me) {
                    send(ELECTION, nextPeer(), round, me, String.valueOf(leader));
                } else if (leader == me) {
                    this.leader = leader;
                    send(ELECTED, nextPeer(), round, me, String.valueOf(leader));
                }
            } else if (type == ELECTED) {
                leader = Integer.valueOf(value);
                int next = nextPeer();
                if (next != leader) {
                    send(ELECTED, next, round, me, String.valueOf(leader));
                }
            } else if (type == EXCHANGE) {
                synchronized (encBallot) {
                    encBallot.put(id, value);
                }
            } else {
                synchronized (messages) {
                    messages.add(new Message(type, round, id, value));
                }
            }
        } catch (Exception e) {
            System.out.println("Failure to handle message");
            e.printStackTrace();
        }
    }

    private int nextPeer() {
        if (me == peers - 1) {
            return 0;
        } else {
            return me + 1;
        }
    }

    private void countVotes() throws RemoteException {
        List<List<Integer>> values = new ArrayList<>(ballot.values());

        List<Integer> result = prunedKY(values);

        List<String> resultString = result.stream().map(String::valueOf).collect(Collectors.toList());

        try {
            broadcast(RESULT, me, String.join(",", resultString));
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unable to broadcast result");
        }
    }

    private void broadcast(MessageType type, int id, String vote) throws RemoteException, MalformedURLException {
        for (int i = 0; i < peers; i++) {
            try {
                send(type, i, round, id, vote);
            } catch (NotBoundException e) {
                System.err.printf("Sending failed on %d\n", i);
                e.printStackTrace();
            }
        }
    }

    private void randomBroadcast(MessageType type, int id, Random rn) throws RemoteException, MalformedURLException {
        for (int i = 0; i < peers; i++) {
            try {
                List<Integer> ballot = createRandomBallot(rn);

                String vote = encrypt(ballot, key);
                send(type, i, round, id, vote);
            } catch (NotBoundException e) {
                System.err.printf("Sending failed on %d\n", i);
                e.printStackTrace();
            }
        }
    }

    private void singleMessage(MessageType type, int to, int round, int from, String vote) throws RemoteException, MalformedURLException {
        try {
            send(type, to, round, from, vote);
        } catch (NotBoundException e) {
            System.err.printf("Sending failed on %d\n", to);
            e.printStackTrace();
        }
    }

    private void send(MessageType type, int dest, int round, int id, String vote)
            throws RemoteException, MalformedURLException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry(basePort + dest);
        ElectorRMI remote = (ElectorRMI) registry.lookup("DemElector");
        remote.handleMessage(type, round, id, vote);
    }

    private void prepareNewRound() {
        synchronized (messages) {
            messages.removeIf(m -> m.round < round);
        }
    }

    public void cleanupMessages(MessageType type) {
        synchronized (messages) {
            messages.removeIf(m -> m.round < round && m.type == type);
        }
    }

    private void await(MessageType type, int peer) throws InterruptedException {
        while (true) {
            if (type == EXCHANGE) {
                synchronized (encBallot) {
                    if (encBallot.size() == peers) {
                        break;
                    }
                }
            } else if (type == ELECTED) {
                if (leader > -1) {
                    break;
                }
            } else if (type == KEY) {
                synchronized (ballot) {
                    if (ballot.size() == peers) {
                        break;
                    }
                }
            } else {
                synchronized (messages) {
                    if (messages.stream().filter(p -> p.type == type && p.round == round && p.peer == peer).count() >= peers) {
                        break;
                    }
                }
            }
            Thread.sleep(100);
        }
    }

    private String encrypt(List<Integer> votes, String key) {
        List<String> voteStrings = votes.stream()
                .map(String::valueOf)
                .collect(Collectors.toList());
        String toEncrypt = String.join(",", voteStrings);

        BasicTextEncryptor crypt = new BasicTextEncryptor();
        crypt.setPassword(key);

        return crypt.encrypt(toEncrypt);
    }

    private List<Integer> decrypt(String encrypted, String key) {
        BasicTextEncryptor crypt = new BasicTextEncryptor();
        crypt.setPassword(key);

        String decrypted = crypt.decrypt(encrypted);

        List<String> voteStrings = Arrays.asList(decrypted.split(","));

        return voteStrings.stream()
                .map(Integer::valueOf)
                .collect(Collectors.toList());
    }


    private String createEncryptionKey() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(128);
            SecretKey key = keyGen.generateKey();

            return Base64.getEncoder().encodeToString(key.getEncoded());
        } catch (NoSuchAlgorithmException ex) {
            // Do nothing
            return "super secret key";
        }
    }

    private MajorityTally getMajorityTally(List<Message> messages, MessageType type, int peer, int round) {
        long maxTally = 0;
        String ballot = "";

        Set<String> differentVotes = messages.stream()
                .filter(p -> p.type == type && p.round == round && p.peer == peer)
                .map(p -> p.vote)
                .collect(Collectors.toSet());

//        System.out.printf("[%d] Majority tally for %d - votes=%s\n", me, peer, differentVotes.toString());
        for (String v : differentVotes) {
            long tally = getTallyOf(messages, type, v, peer, round);
//            System.out.printf("[%d] Tally for %d - %s = %d\n", me, peer, v, tally);
            if (tally > maxTally) {
                maxTally = tally;
                ballot = v;
            }
        }

        return new MajorityTally(ballot, maxTally);
    }

    private static long getTallyOf(List<Message> messages, MessageType type, String ballot, int peer, int round) {
        return messages.stream()
                .filter(p -> p.type == type && p.round == round && p.peer == peer && ballot.equals(p.vote))
                .count();
    }

    private List<Integer> createRandomBallot(Random rn) {
        List<Integer> ballot = new ArrayList<>();
        for (int i = 0; i < candidates; i++) {
            ballot.add(i);
        }

        Collections.shuffle(ballot, rn);

        return ballot;
    }

    private List<Integer> prunedKY(List<List<Integer>> voterPreferences) {
        List<List<Integer>> rankPermutations = permute(candidates);

        int maxScore = -1;
        List<Integer> maxRank = null;

        voterPreferences = pruneDistanceRank(voterPreferences);
        for (int i = 0; i < rankPermutations.size(); i++) {
            int score = 0;
            // Sum scores that match both predicted ranked preference and ballot voter preference
            // Sum over each pair (a, b) in predicted rank, where a > b
            for (int j = 0; j < rankPermutations.get(i).size(); j++) {
                for (int k = j + 1; k < rankPermutations.get(i).size(); k++) {
                    // Sum over each ballot
                    for (int l = 0; l < voterPreferences.size(); l++) {
                        if (compareCandidate(rankPermutations.get(i).get(j), rankPermutations.get(i).get(k), voterPreferences.get(l))) {
                            score++;
                        }
                    }
                }
            }

            if (score > maxScore) {
                maxScore = score;
                maxRank = rankPermutations.get(i);
            }
        }

        return maxRank;
    }

    private static List<List<Integer>> permute(int n) {
        List<List<Integer>> ll = new ArrayList<>();
        List<Integer> l = new ArrayList<>();
        ll.add(l); // Initialize with empty list
        for (int i = 0; i < n; i++) {
            List<List<Integer>> ll2 = new ArrayList<>();
            for (int j = 0; j < ll.size(); j++) {
                l = ll.get(j);
                for (int k = 0; k <= l.size(); k++) {
                    List<Integer> l2 = new ArrayList<>(l); // Copy
                    l2.add(k, i); // Insert at index j
                    ll2.add(l2); // Append to new list
                }
            }
            ll = ll2; // Repeat next iteration with new list
        }

        return ll;
    }

    private static List<List<Integer>> pruneDistanceRank(List<List<Integer>> preferences) {
        int[] distances = new int[preferences.size()];
        for (int i = 0; i < preferences.size(); i++) {
            for (int j = i + 1; j < preferences.size(); j++) {
                // Sum distance over each pair of candidate, (candidate_k, candidate_l)
                for (int k = 0; k < preferences.get(i).size(); k++) {
                    for (int l = k + 1; l < preferences.get(i).size(); l++) {
                        // Increase distance if preference differs between 2 voters
                        boolean voterPref1 = compareCandidate(k, l, preferences.get(i));
                        boolean voterPref2 = compareCandidate(k, l, preferences.get(j));
                        if (voterPref1 != voterPref2) {
                            distances[i]++;
                            distances[j]++;
                        }
                    }
                }
            }
        }

        List<Pair<Integer, Integer>> pairs = getRank(distances, false);

        int prunedSize = preferences.size() / 2; // or voterPrefList.length * 2 / 3
        List<List<Integer>> prunedVoterPrefList = new ArrayList<>();
        for (int i = 0; i < prunedSize; i++) {
            Pair<Integer, Integer> pair = pairs.get(i);
            int key = pair.getKey();
            prunedVoterPrefList.add(preferences.get(key));
        }

        return prunedVoterPrefList;
    }

    private static boolean compareCandidate(int candidate1, int candidate2, List<Integer> voterPref) {
        // Start from the beginning of preference list
        // If first candidate has higher rank, return true
        // Otherwise, return false
        for (int i = 0; i < voterPref.size(); i++) {
            if (voterPref.get(i) == candidate1)
                return true;
            else if (voterPref.get(i) == candidate2)
                return false;
        }
        return false; // Never
    }

    private static List<Pair<Integer, Integer>> getRank(int[] scores, boolean reversed) {
        // Sort rank descending
        List<Pair<Integer, Integer>> pairs = new ArrayList<>();
        for (int i = 0; i < scores.length; i++) {
            pairs.add(new Pair<>(i, scores[i]));
        }
        if (reversed) {
            Collections.sort(pairs, (p1, p2) -> p2.getValue() - p1.getValue()); // descending
        }
        else {
            Collections.sort(pairs, (p1, p2) -> p1.getValue() - p2.getValue()); // ascending
        }

        return pairs;
    }
}
